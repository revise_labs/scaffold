@echo off
set install_path=%~dp0
git clone "%install_path%templates\%1" "%2"
if exist %2\.git (
echo Resetting git repository...
rmdir /S /Q %2\.git
echo Done.
)
