# Scaffold

Scaffold is a Windows utility primarily designed for Play! Framework developers. Create a local repository of project templates use those templates when starting a new project instead of starting from scratch.

Scaffold requires Git for Windows.

### Clone the repository and make a template directory
```
git clone https://revise_labs@bitbucket.org/revise_labs/scaffold.git
cd scaffold
mkdir templates
```

Be sure to add `scaffold.bat` to your Windows PATH.

### Add templates

Add project templates to the `scaffold\templates` folder (ensure that the template folder names have no spaces). For each template, initialize a Git repository and make an initial commit:

```
cd scaffold\templates\play-23
git init .
git commit -am "Initial commit"
```

### Scaffold projects

Use the templates you add to scaffold new projects.

```
scaffold play-23 new-app
```

